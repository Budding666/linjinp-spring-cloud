# spring-cloud
博客

https://blog.csdn.net/qq_37143673

练习框架

子项目：

common：项目公共部分，工具，通用接口等

service-a：服务A（8080）

service-b：服务B（8081）

feign-demo：feign 调用示例以及异常拦截处理（8082）

ribbon-demo：ribbon + hystrix 调用示例（8083）

zuul：网关示例，配置，拦截器，回退等（8084）

config：配置中心服务端（8085）

activiti-demo：Activiti6 工作流引擎示例 + Activiti Modeler 流程配置可视化（8086）

Flowable-demo：Flowable 工作流引擎示例 + Flowable Modeler 流程配置可视化（8087）

redis-demo：redis 集成示例（8088）

kafka-demo：kafka 集成，生产者，消费者示例（8089）

eureka：eureka 集群（8001，8002，8003）

Zipkin-Server：Zipkin服务端（9411）http://localhost:9411/

quartz：任务调度框架（8090）

其他：

mysql：master（7000），slave（7001）  账号/密码：root/123456

停用 Atlas（1234）：mysql 主从库负载  账号/密码：root/123456

Mycat（8066开发，9066管理）：mysql 主从库负载，主备切换  账号/密码：root/123456

Jenkins（7005）：持续集成  账号/密码：root/123456

nacos（8848）：nacos 注册中心  账号/密码：nacos/nacos

redis：sentinel 集群（6379，6380，6381） cluster 集群（6382，6383，6384）
